#!/bin/bash

set -u

# fgrestore - Automate a manual restore of a backup created with bgbackup script. Great for refreshing a replica.

[[ -f "/etc/redhat-release" ]] \
  && default_my_cnf_file="/etc/my.cnf" \
  || default_my_cnf_file="/etc/mysql/my.cnf"
default_sleep_time="5s"

# Handle control-c
function sigint {
  echo "User has canceled with control-c."
  # 130 is the standard exit code for SIGINT
  exit 130
}

function set_fs_permissions() {
  local fs_object="$1"
  echo -n "  '${fs_object}' ... changing ownership"
    $dry_run chown -R mysql: "$fs_object"
  if [[ ${run_restorecon} -ne 0 ]] ; then
    echo -n " ... running restorecon"
      $dry_run restorecon -rvi "${fs_object}"
  fi
  echo " ... finished"
}

function usage() {
   echo "Syntax: $0"
   echo "Defining source and target:"
   echo "    -S                              Source folder of backup"
   echo "    -D                              Destination folder (where to restore)"
   echo ""
   echo "Parameters for the restore commands"
   echo "    -p                              number of Parallel threads to use during restore. In this script, default is 4 (overriding factory default 1)."
   echo "                                     details: https://docs.percona.com/percona-xtrabackup/8.0/xtrabackup-option-reference.html#parallel"
   echo "    -m                              amount of Memory to use during restore."
   echo "                                    The default value is 100MB. The recommended value is between 1GB to 2GB."
   echo "                                    Multiple values are supported if you provide the unit (for example, 1MB, 1M, 1GB, 1G)."
   echo "                                     details: https://docs.percona.com/percona-xtrabackup/8.0/xtrabackup-option-reference.html#use-memory"
   echo "    -r                              use decompressing with '--remove-original' parameter"
   echo "                                     details: https://docs.percona.com/percona-xtrabackup/8.0/xtrabackup-option-reference.html#remove-original"
   echo "    -M                              after preparing, run '--move-back'. Defaults cnf file can be set by using '-C' switch"
   echo "                                     details: https://docs.percona.com/percona-xtrabackup/8.0/xtrabackup-option-reference.html#move-back"
   echo "    -C <defaults_cnf_file>          use 'defaults_cnf_file' for db configuration. If not set, default value of '${default_my_cnf_file}' will be used."
   echo ""
   echo "Additional options:"
   echo "    -N                              non-interactive mode. If activated, script will only sleep '${default_sleep_time}', not waiting for keyboard input"
   echo "    -R                              after all the steps, not only run 'chown' on instance folders, but also 'restorecon -rvi'."
   echo "                                     This have to be used while SELinux is configured, mostly on RedHat based systems."
   echo ""
   echo "Script config options:"
   echo "    -h                              Print this Help."
   echo "    -d                              Instead of running commands, print them (whole command lines)."
   echo "                                     A sort of dry-run, good for reviewing and manual run of commands (with the evaluated variables)."
   echo

   exit 1
}

backup_path=""
restore_path=""
parallel="4"
mem_to_use="100M"
remove_original=""
run_move_back=0
my_cnf_file=""
non_interactive=0
run_restorecon=0
dry_run=""
while getopts S:D:p:m:rMC:NRhd option; do
  case "$option" in
    S)  backup_path=$(cd "$OPTARG" > /dev/null; pwd -P )
        ;;
    D)  restore_path="$OPTARG"
        ;;
    p)  parallel="$OPTARG"
        ;;
    m)  mem_to_use="$OPTARG"
        ;;
    r)  remove_original=" --remove-original "
        ;;
    M)  run_move_back=1
        ;;
    C)  my_cnf_file="$OPTARG"
        ;;
    N)  non_interactive=1
        ;;
    R)  run_restorecon=1
        ;;
    h)  usage
        ;;
    d)  dry_run="echo "
        ;;
    *)  usage
        ;;
  esac
done
my_cnf_file=${my_cnf_file:-$default_my_cnf_file}
my_cnf_file_includes=$(grep -E '^[[:space:]]*!((include)|(includedir))[[:space:]]' ${my_cnf_file} | awk '{print $2}')
my_cnf_grep_target="${my_cnf_file} ${my_cnf_file_includes}"
sleep_time=${sleep_time:-$default_sleep_time}

[[ ! -d ${backup_path} ]] && echo "[ERROR] missing backup folder to restore from ('${backup_path}')!" && usage

if [ ! -f "$backup_path/bgbackup.cnf" ]; then
    echo "$backup_path/bgbackup.cnf does not exist. $0 only works with newest version of bgbackup."
    exit 1
fi

echo "Reading configuration from $backup_path/bgbackup.cnf"
source $backup_path/bgbackup.cnf

echo "Creating commands based on configuration"
if [ "$backuptool" = 2 ]; then
    innocommand="xtrabackup"
else
    innocommand="mariabackup"
fi

cat << EOFEOF
Configuration is:
  backup_path='${backup_path}'
  restore_path='${restore_path}'
  innocommand='${innocommand}'
  parallel='${parallel}'
  mem_to_use='${mem_to_use}'
  remove_original='${remove_original}'
  run_move_back='${run_move_back}'
  my_cnf_file='${my_cnf_file}'
  my_cnf_grep_target='${my_cnf_grep_target}'
  non_interactive='${non_interactive}'
  sleep_time='${sleep_time}'
  run_restorecon='${run_restorecon}'
  dry_run='${dry_run}'
EOFEOF
echo "Sanity checks and notices..."
if [ -z "$(command -v $innocommand)" ]; then
    echo "$innocommand does not exist"
    exit 1
fi

if [ -z "$(command -v zstd)" ]; then
    echo "zstd command does not exist, try: yum install zstd, or: apt install zstd"
    exit 1
fi

current_xtrabackup_version=$($innocommand --version 2>&1 | grep based)
current_server_version=$(mysqld -V 2>/dev/null)

if [ "$current_server_version" != "$server_version" ]; then
        echo "WARNING! Backup was created from a different server version. This should be backwards compatible (newer version is okay) but there might be bugs. If there are any issues, please install the same version."
        echo ""
        echo "Backup server version: $server_version"
        echo "Current server version: $current_server_version"
else
        echo "Server version is correct"
fi
if [ "$current_xtrabackup_version" != "$xtrabackup_version" ]; then
        echo "WARNING! Backup was created from a different xtrabackup version. f there are any issues, please install the same version."
        echo "Backup xtrabackup version: $xtrabackup_version"
        echo "Current xtrabackup version: $current_xtrabackup_version"
else
        echo "Xtrabackup version is correct"
fi

if [[ ! ( -d "${restore_path}" ) ]] ; then
    $dry_run mkdir -p $restore_path
else
    echo -n "Clearing out $restore_path/*"
    if [[ $non_interactive -eq 0 ]] ; then
      echo " (press Y/y to continue)"
      echo -n "Are You sure? (y/n) "
      read _answer

      [[ ! ( ${_answer} =~ ^[Yy]$ ) ]] && echo "Cancelled, exiting." && exit 0
    else
      echo " (ctrl+C within ${sleep_time} to cancel)"
      sleep ${sleep_time}
    fi

    $dry_run rm -rf "$restore_path/*"
fi
restore_path=$(cd ${restore_path} > /dev/null; pwd -P )

echo "Copying backup to restore location"
$dry_run cp -a $backup_path/* $restore_path/

if [ "$encrypt" = "yes" ]; then
    echo "Decrypting..."
    $dry_run $innocommand --decrypt=AES256 --encrypt-key="$(cat "$cryptkey")" --parallel=$parallel --target-dir="$restore_path"
fi

if [ "$compress" = "yes" ]; then
    echo "Decompressing..."
    $dry_run $innocommand --decompress ${remove_original} --parallel=$parallel --target-dir="$restore_path"
fi

echo "Preparing..."
$dry_run $innocommand --prepare --use-memory=${mem_to_use} --parallel=$parallel --target-dir="$restore_path"

if [ ${run_move_back} -ne 0 ]; then
    echo "Moving back to configured folders..."
    echo "Run --move-back means that folders configured in '${my_cnf_file}' have to be cleared."
    if [[ $non_interactive -eq 0 ]] ; then
      echo -n "Is this OK? (press Y/y to continue) "
      read _answer

      [[ ! ( ${_answer} =~ ^[Yy]$ ) ]] && echo "Cancelled, exiting." && exit 0
    else
      echo " (ctrl+C within ${sleep_time} to cancel)"
      sleep ${sleep_time}
    fi

    for _pid in $(ps xa | grep -v grep | grep "mysqld.*--defaults-file=${my_cnf_file} " | awk '{print $1}') ; do
      echo "Stopping pid ('kill ${_pid}')..."
      $dry_run kill ${_pid}
      [[ "$(ps -eo pid | grep -v grep | grep ${_pid})" != "" ]] && echo "Didn't stop, forced killing pid ('kill -9 ${_pid}')..." && $dry_run kill -9 ${_pid}
    done
    # _config_folder_param_list: this list holds items which can define filesystem resources in db config files. The list can be used for setting ownerships and permissions for db related filesystem resources
    #   TODO: review list of configuration parameters which can hold folder names to manage (chown and SELinux/restorecon)
    _config_folder_param_list='^[[:space:]]*((datadir)|(innodb.data.home.dir)|(tmpdir)|(log.bin)|(innodb.log.group.home.dir)|(relay.log)|(innodb.doublewrite.dir)|(replica.load.tmpdir))[[:space:]=]'

    # _config_folder_list: values of the config parameters defined in the previous list
    #   as former list filtered out config items starting with comment mark ('#'), we don't have to count on any commented parameter
    _config_folder_list=$(grep -Er "${_config_folder_param_list}" ${my_cnf_grep_target} | sed -e '1,$ s@.*=[[:space:]]*@@' | sort -u)

    for _config_folder in ${_config_folder_list} ; do
      $dry_run rm -rf ${_config_folder}
      $dry_run mkdir -p ${_config_folder}
    done

    _config_file_param_list='^[[:space:]]*(log-error)[[:space:]=]'
    _config_file_list=$(grep -Er "${_config_file_param_list}" ${my_cnf_grep_target} | sed -e '1,$ s@.*=[[:space:]]*@@' | sort -u)
    for _config_file in ${_config_file_list} ; do
      _config_file_folder=$(dirname ${_config_file})
      $dry_run rm -rf ${_config_file_folder}
      $dry_run mkdir -p ${_config_file_folder}
    done

    $dry_run $innocommand --defaults-file=${my_cnf_file} --move-back --parallel=$parallel --target-dir="$restore_path"
fi

echo "Changing ownership and running 'restorecon' (if configured)"
set_fs_permissions "$restore_path"
if [ ${run_move_back} -ne 0 ]; then
  for _config_folder in ${_config_folder_list} ; do
    set_fs_permissions "${_config_folder}"
  done
  for _config_file in ${_config_file_list} ; do
    _config_file_folder=$(dirname ${_config_file})
    set_fs_permissions "${_config_file_folder}"
  done
fi

echo "You can now start MySQL/MariaDB/Percona"
echo "Binary log information:"
$dry_run cat "$restore_path/xtrabackup_binlog_info"

