#!/bin/bash

oldpwd=$(pwd)

cd $(dirname $0)

anticache_string=`head -1 <(fold -w 20  <(tr -dc 'a-zA-Z0-9' < /dev/urandom))`

if [ -f ./bgbackup.sh ]; then
    rm -rf /tmp/bgbackup.sh
    mv bgbackup.sh /tmp/
fi

wget --no-cache "https://raw.githubusercontent.com/michaeldg/bgbackup/master/bgbackup.sh?no-cache=${anticache_string}" -O bgbackup.sh
chmod +x bgbackup.sh

echo "Downloaded the latest bgbackup"

if [ -f ./bgrestore.sh ]; then
    rm -rf /tmp/bgrestore.sh
    mv bgrestore.sh /tmp/
fi

wget --no-cache "https://raw.githubusercontent.com/michaeldg/bgrestore/master/bgrestore.sh?no-cache=${anticache_string}" -O bgrestore.sh
chmod +x bgrestore.sh

echo "Downloaded the latest bgrestore"

cd $oldpwd
