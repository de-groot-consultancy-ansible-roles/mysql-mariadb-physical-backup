# MySQL or MariaDB physical backup and automated restores

- This role installs [bgbackup](https://github.com/michaeldg/bgbackup), an open-source wrapper script for mariabackup and xtrabackup.
- For automated restores this role installs [bgrestore](https://github.com/michaeldg/bgrestore), an open-source script that restores the backup (using mariabackup or xtrabackup and normal shell commands)
- To transfer the backup this role creates and replaces an SSH key pair and installs a script 'copy-last-backup.sh' to copy the the last backup (if it was a full backup) to the restore machines
- Please check `defaults/main.yml` for a full list configuration options and more documentation


# Example configuration to create backup
```
bgbackup_backup_path: "/backupdisk"
bgbackup_hour: "*" # Executing bgbackup every hour creates a differential backup based on the full backup of the beginning of the day.
bgbackup_create_history_schema: True # this is actually the default, so you can skip configuring it
bgbackup_create_backup_user: True # this is actually the default, so you can skip configuring it
bgbackup_backup_password: False # If this is not specified, MySQL and Percona will use auth_socket authentication, for MariaDB instances we generate a random password.
bgbackup_backup_priviledged_host: "localhost" # Default
bgbackup_backup_priviledged_user: "root" # Default
bgbackup_backup_priviledged_password: False # The default, which uses auth_socket authentication in MySQL, or unixsocket authentication in MariaDB. Alternatively you can configure a password.
bgbackup_create_backup_history_user: True  # The same applies here if the backup history runs on localhost, specify no password and we use auth_socket authentication or a random password.
bgbackup_history_host: "localhost" # this is actually the default, so you can skip configuring it
bgbackup_history_priviledged_user: "root" # by default it takes the same username as `bgbackup_backup_priviledged_host`
bgbackup_slave_on: "no" # The default
```

# Backup history and frequency
By default, 3 full backups and all their incrementals/differentials are kept (`bgbackup_backup_keep_num`). Next to that, 1000 log files (`bgbackup_keep_log_num`) are kept. To create differentials specify more then 1 backup per day by passing _cron_ comptabie hour string to `bgbackup_hour`, for example '3,6,9,12,15,18,21,0' or '*'. The first backup of the day is a full backup, the rest of the backups are differentials (a differential is the difference between the last full backup and the current backup moment, an incremental is the difference between the last backup (incremental or full) and the current backup moment.

The backup history writes successful backups to a history table. The content of this table is not needed to restore a backup. In theory the backup system could run without the history system enabled but it is not supported yet (pull requests are welcome). The history data is used to create incrementals and differentials and for the cleanup phase where old backups are deleted.

# Backup history on different server
When backing up multiple instances it is recommended to centrally store the backup history. This central location can be used for monitoring of the backup result and alert when the backup did not run. To configure that, specify:
```
bgbackup_history_host: "1.2.3.4" # the IP of the backup history server
bgbackup_history_priviledged_delegate_to: "inventory_hostname of backup history server"  # This defaults to `backup_history_host`, so not needed if both are the same
bgbackup_recycle_and_generate_history_password: True # This is enabled by default, unless backup (history)  is running as root, or with mysql authsocket authentication
bgbackup_history_priviledged_password: False # The default, which uses auth_socket authentication in MySQL, or unixsocket authenticaiton in MariaDB
bgbackup_history_priviledged_user: "root" # The default
bgbackup_history_user_source_host: "{{ inventory_hostname }}"  # The default, an account in MySQL is user@host. So this way each instance that writes history has a different password.
bgbackup_galera_on: "yes" # The default, check if this is a Galera node, and if so enabled `wsrep_desync` for the time the backup is being created.
```

# MariaDB, MySQL and Percona support
The role and bgbackup supports both MariaDB (up to 10.11, newer versions might work) and MySQL (8.0, newer versions might work). MariaDB is the default, specify `bgbackup_flavor` to 'mysql' or 'percona' to enable mysql support (the default is 'mariadb').

# Authentication for backup user
We recommend unixsocket authentication for the backup process. This way, no password needs to be stored anywhere and the backup user is as safe as the root user on the OS.

In MariaDB, unixsocket authentication can only work from the same OS user, so specify 'root' as the backup user without a password. If you specify another user then root for backups but not a password, the user is created and it's password is changed to something random and safe each time the automation runs.

In MySQL, auth_socket authentication is assumed if no password is specified. The auth_socket plugin is added and the user is created with auth_socket authentication. Do you prefer a random password to be creatde? Specify `bgbackup_recycle_and_generate_backup_password`.

If you don't want the automation to create the backup user for you, specify `bgbackup_create_backup_user` to False. The functionality is enabled by default and we recommend to use it for better security.

# Credential files
By default, the automation writes a credentials file into /root/.my.bgbackup-backup.cnf and /root/.my-bgbackup-history.cnf. These contains everything required to connect to the database. All options except the password are passed on the CLI as well, so if anything changes in the configuration roll this out with ansible to keep everything consistent.

# Multi-instance support
To support multiple instances, please specify at least `bgbackup_backup_defaults_file`. The instance name, configurable through `bgbackup_instance_name` by default becomes the file name of the defaults file if it is specified. The instance_name will be used for log file name, configuration file name, log file path, backup path and the file name of the credential and pid files. Next to that, the backup instance name is used in the hostname column of the backup history table.

We recommend storing the backup history remotely for easy monitoring of the results of the backup system. In this case we recommend including the `bgbackup_instance_name` into the value of `bgbackup_history_user`, for example by configuring it as 'backuphistory_{{ bgbackup_instance_name }}' - this will avoid breaking the authentication for other instancees on the same host. If you don't use a host-specific `bgbackup_history_user_source_host` (the default (when backup history configured to run on a remote host) is the `ansible_host` or `inventory_hostname` of the backup target host.

# Preflight checks
We write a pid file and verify that only process is running. If the pid file is not written the first 5 minutes of the script's execution the backup is not run.

Next to that it is possible to configure preflight checks through a shell command. This allows you to run backups on 2 nodes in a cluster, except the one currently receiving traffic. Or, you can check if not too many bgbackup instances are running. For example:
```
bgbackup_preflight_script: >
  if [ $(mysql --defaults-file={{ bgbackup_defaults_file | quote }} --user=root_automation -Bse 'SHOW SLAVE HOSTS'|wc -l) != '0' ]; then echo 'NO_ERROR;Not creating a backup, because there are replicas connected';
  elif [ $(ps aux|grep bgbackup|grep -v grep|grep bash|wc -l) -gt 2 ]; then echo 'Not creating a backup, at least 2 other bgbackup processes are already running';
  else echo 'OK'; fi`
```

If the preflight check script echoes 'OK', it indicates that all checks have passed. If it echoes something else or nothing it indicates an error, backup will not run and history is not written. You can control the return code of the program by prefixing the nessage with 'NO_ERROR;' - this makes the bgbackup process exit without an error code.

PLEASE NOTE: No special escaping is implemented. This means any subcommands in parentheses $(mysql ..._) are executed when the configuration is loaded. This could become problematic when writing complex scripts with loops and re-evaluation of such subcommnand. Pull requests are welcome.

# Example configuration to transfer and do automated restores
```
bgbackup_restore_target_directory: "/backup_to_restore/"
bgbackup_automated_restores: True
bgbackup_restore_machine: "10.10.18.154"
bgbackup_restore_ssh_user: "bgrestore"
bgrestore_empty_authorized_keys_file: True
bgrestore_target_directory: "/var/lib/mysql"
```

Each time the automation runs, a new SSH key is generated and stored on the automated restore host.

# Monitoring

**Automatic monitoring is not enough. Please verify your backups at least twice the retention time.**<br />
Use this query for your monitoring, to monitor the size of the last backup created in the last 36 hours:<br />
`SELECT SUM(backup_size) AS backup_size FROM (SELECT 0 AS backup_size UNION (SELECT CAST(REPLACE(REPLACE(backup_size, 'M', ''), 'G', '000') AS INT) FROM backup_history WHERE start_time > NOW() - INTERVAL 36 HOUR AND hostname=(SELECT @@hostname) order by start_time DESC LIMIT 1)) a;`

# Space requirements
The automated restore node requires 2x the size of the data directory (excluding binary logs and gcache) of the node where backups are created.
The space can be decreased to roughly 1.5x the size of the data directory by enabling `bgrestore_delete_previous_backup_first` (actual size requirement = 1x data dir size + 1x compressed data dir szie).

# Todo
- Improve security (does not have to run as root, for example: https://github.com/esoucy19/ansible-role-mariabackup/blob/master/tasks/main.yml)
- Role documentation
- Better testing

# Pull requests

PR's are very welcome, this role is actively maintained!
