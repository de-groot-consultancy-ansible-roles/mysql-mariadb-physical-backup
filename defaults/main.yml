bgbackup_flavor: "mariadb"
bgbackup_backup_tool: "{{ 1 if bgbackup_flavor == 'mariadb' else 2 }}"  # 1 for Mariabackup, 2 for xtrabackup

# Multi-instance support: The instance_name is used in the backup history
# table. Specify a different defaults file to create the backup from a
# multi-instance mysql/mariadb server
bgbackup_defaults_file: False # Default: OS default
bgbackup_instance_name: "{{ bgbackup_defaults_file | replace('/etc/mysql/', '') | replace('/etc/', '') | replace('.cnf', '') if bgbackup_defaults_file not in (False, '/etc/my.cnf', '/etc/mysql/my.cnf', '/etc/mariadb.cnf', '/etc/mysql/mariadb.cnf', '/etc/mariadb/mariadb.cnf') else False }}"

# When possibly creating backups from multiple instances (for example an
# asynchronous replication pair, where some times backups are created on the
# other host) the backups on sibling hosts may need to be removed as well as
# the backups from this host.
# 
# Specify a comma separated list fo siblings. Don't forget the instance_name if added with a -.
# The host name taken is the exact output of `hostname`.
bgbackup_siblings: False
# When siblings are specified, backups of the siblings are rotated as well, and
# considered it's own backups. Incrementals/differentials can only be created
# from this node's backups.
#
# WARNING! The backups of all the siblings MUST reside on the exact same
# storage as the backup of this host. Otherwise, deleting of sibling's backups
# will stop the sibling from deleting the backups.
#
# If you want to always create backups on all siblings, do not use this
# feature. The backups will be rotated in each node's own backup process.

# Defaults extra file can be used for authentication (avoid password in the CLI). However, we recommend against this because this implementation will overwrite entire sections, making it necesarry to specify some parameters twice (Don't Repeat Yourself, against DRY coding standards). However, use it as you like. The defaults file is loaded first, and then the defaults-extra-file is loaded.A
bgbackup_defaults_extra_file: False

# To avoid specifying passwords on the command-line, we write out a credentials_file
bgbackup_backup_credentials_file: "/root/.bgbackup-backup{{ '-' + bgbackup_instance_name if bgbackup_instance_name else '' }}.cnf"
bgbackup_backup_write_credentials_file: "{{ True if 'bgbackup-backup' in bgbackup_backup_credentials_file | default(False) else False }}"

bgbackup_cron_file_name: "/etc/cron.d/bgbackup{{ '-' + bgbackup_instance_name if bgbackup_instance_name else '' }}"
bgbackup_config_file_name: "/etc/bgbackup{{ '-' + bgbackup_instance_name if bgbackup_instance_name else '' }}.cnf"

bgbackup_open_files_limit: 20000  # We recommend to allow bgbackup to open as many files as needed. Can be limited for more safety when the importance of the machine being backed up justifies limiting this.

# Bgbackup should run on the system that is backed up.
bgbackup_backup_host: False # Automatically from the defaults file
bgbackup_backup_port: False # Automatically from the defaults file
bgbackup_backup_unix_socket: False # Automatically from the defaults file
bgbackup_backup_user: "root"  # By default, backup runs as root user with mariadb unixsocket authentication.
bgbackup_backup_password: False

# MySQL auth_socket plugin supports logging in as a different user then the
# user in the operating system. This is the recommended way to safely
# authenticate bgbackup on MySQL systems.
bgbackup_backup_mysql_auth_socket_authentication_enabled: "{{ 1 if bgbackup_flavor in ('mysql', 'percona') and not bgbackup_backup_password else 0 }}"

# If bgbackup does not run as root, and no password is specified, we
# auto-generate a backup password and recycle it every time the automation is
# installed.
bgbackup_recycle_and_generate_backup_password: "{{ 1 if not bgbackup_backup_mysql_auth_socket_authentication_enabled or (bgbackup_flavor == 'mariadb' and not bgbackup_backup_password and bgbackup_backup_user != 'root') else 0 }}"

bgbackup_create_backup_user: True
bgbackup_backup_user_source_host: "localhost"

bgbackup_backup_priviledged_defaults_file: "{{ bgbackup_defaults_file | default(False) }}"
bgbackup_backup_priviledged_defaults_extra_file: "{{ bgbackup_defaults_extra_file | default(False) }}"
bgbackup_backup_priviledged_host: "{{ bgbackup_backup_host | default(False) }}"
bgbackup_backup_priviledged_port: "{{ bgbackup_backup_port | default(False) }}"
bgbackup_backup_priviledged_unix_socket: "{{ bgbackup_backup_unix_socket | default(False) }}"
bgbackup_backup_priviledged_user: "root"
bgbackup_backup_priviledged_password: ""  # Empty password in case of authsocket authentication

# Backup user will be recycled every run, creating transactions on the backup host. When using GTID replication this generated a GTID. This can be undesirable, if the replication is not circular there will be an error after some switchovers. 
# Instead of setting the priviledges host to the primary you can also disable binary logging. Or, any other CLI switch you  wish to use.
bgbackup_backup_priviledged_pre_sql_commands: ""
# For example: "SET wsrep_on=0; SET sql_log_bin=0;"
# WARNING: History will get the same setting by default

# If the backup history is a multi-instance server as well (perhaps a different
# instance, or (default) the same instnace as the backup host) allow specifying
# a different defaults file
bgbackup_history_defaults_file: "{{ bgbackup_defaults_file | default(False) }}"   # False: The OS default

# Defaults extra file can be used for authentication (avoid password in the CLI). However, we recommend against this because this implementation will overwrite entire sections, making it necesarry to specify some parameters twice (Don't Repeat Yourself, against DRY coding standards). However, use it as you like. The defaults file is loaded first, and then the defaults-extra-file is loaded.A
bgbackup_history_defaults_extra_file: "{{ False if bgbackup_history_user != bgbackup_backup_user else bgbackup_defaults_extra_file  }}"

# To avoid specifying passwords on the command-line, we write out a credentials_file
bgbackup_history_credentials_file: "/root/.bgbackup-history{{ '-' + bgbackup_instance_name if bgbackup_instance_name else '' }}.cnf"
bgbackup_history_write_credentials_file: "{{ True if 'bgbackup-history' in bgbackup_history_credentials_file else False }}"

# Which host the backup tool can connect to to store backup history. If you change this to non-default, the same host will be used for priviledged backup history tasks (like creating the backup history user). Make sure to set up `bgbackup_history_priviledged_delegate_to` as well if that host name is different for ansible.
bgbackup_history_host: "{{ bgbackup_backup_host | default(False) }}"
bgbackup_history_port: "{{ bgbackup_backup_port | default(False) }}"
bgbackup_history_unix_socket: "{{ bgbackup_backup_unix_socket | default(False) }}"
bgbackup_history_user: "{{ bgbackup_backup_user | default('root') }}"
bgbackup_history_password: "{{ False if bgbackup_history_user != bgbackup_backup_user else bgbackup_backup_password }}"
bgbackup_history_schema: "dba"

# MySQL auth_socket plugin supports logging in as a different user then the
# user in the operating system. This is the recommended way to safely
# authenticate bgbackup on MySQL systems.
bgbackup_history_mysql_auth_socket_authentication_enabled: "{{ 1 if bgbackup_flavor in ('mysql', 'percona') and not bgbackup_history_password and bgbackup_history_user != bgbackup_backup_user and bgbackup_history_host not in (False, 'localhost') else 0 }}"

# If a unique username is used for backup history, bgbackup does not run as root, and no password is specified, we
# auto-generate a backup password and recycle it every time the automation is
# installed.
bgbackup_recycle_and_generate_history_password: "{{ 1 if not bgbackup_history_mysql_auth_socket_authentication_enabled or (bgbackup_flavor == 'mariadb' and not bgbackup_history_password and bgbackup_history_user != 'root') and bgbackup_history_user != bgbackup_backup_user and not bgbackup_history_password else 0 }}"

# Extra priviledged credentials can be supplied to login to the database to create the schema and run maintenance query from the automation. By defaul the history credentials will be used:
bgbackup_history_priviledged_defaults_file: "{{ bgbackup_history_defaults_file }}"
bgbackup_history_priviledged_defaults_extra_file: "{{ bgbackup_history_defaults_extra_file }}"
bgbackup_history_priviledged_delegate_to: "{{ bgbackup_history_host or inventory_hostname }}"
bgbackup_history_priviledged_port: "{{ bgbackup_history_port }}"
bgbackup_history_priviledged_unix_socket: "{{ bgbackup_history_unix_socket }}"
bgbackup_history_priviledged_user: "{{ bgbackup_backup_priviledged_user }}"
bgbackup_history_priviledged_password: "{{ bgbackup_backup_priviledged_password }}"

# Backup user will be recycled every run, creating transactions on the backup host. When using GTID replication this generated a GTID. This can be undesirable, if the replication is not circular there will be an error after some switchovers. 
# Instead of setting the priviledges host to the primary you can also disable binary logging. Or, any other CLI switch you  wish to use.
bgbackup_history_priviledged_pre_sql_commands: "{{ bgbackup_backup_priviledged_pre_sql_commands }}"
  # For example: "SET wsrep_on=0; SET sql_log_bin=0;"

bgbackup_create_history_schema: True
bgbackup_create_history_user: True
bgbackup_history_user_source_host: "{{ 'localhost' if bgbackup_history_host == 'localhost' else (ansible_host | default(inventory_hostname)) }}"

# Whether or not to verify backup history is working correctly
# By default verrfication is enabled: Bgbackup will not run if backup history
# host is unreachable, or if the backup history schema does not exist.
# Incrementals and differentials are only possible with backup history.
# Otherwise all backups are full backups.
bgbackup_history_verify: 1

# Path where to store the backups
bgbackup_backup_path: "/var/backups/bgbackup{{ '/' + bgbackup_instance_name if bgbackup_instance_name else '' }}"

# Path where to create the logs
bgbackup_log_path: "/var/logs/bgbackup{{ '/' + bgbackup_instance_name if bgbackup_instance_name else '' }}"

# Full backup timing
# Can be: Monday, Tuesday, Wednesday, Thursday, Friday, Saturday, Sunday, Everyday, Always
bgbackup_full_backup_timing: "Everyday"

# Email address to send notifications to
bgbackup_notification_emails: "michael@chipta.com"

# Email subject prefix
bgbackup_email_sub_pre: "[BGBackup]"

# This preflight script will return OK. If you decide to add your own preflight script, make sure the output is only "OK" and nothing else
bgbackup_preflight_script: "echo 'OK' || echo"

# Enable debug mode (more output from bgbackup)
bgbackup_debug_mode: "no"

# Enable verbose mode (log output to stdout as well as log file)
bgbackup_verbose_mode: "{{ bgbackup_debug_mode }}"

# Should the automation create a backup when a new configuration file is deployed?
bgbackup_automation_configuration_backup: "{{ bgbackup_debug_mode }}"

bgbackup_galera_on: "yes"
bgbackup_galera_minimum_nodes: "0"
bgbackup_slave_on: "no"
bgbackup_rocksdb_on: "no"

bgbackup_minute: "7"
bgbackup_hour: "2"

# How many full backups to keep (incrementals and differentials are removed when the corresponding full backup is removed"
bgbackup_keep_daily: "{{ bgbackup_keep_num | default(3) }}"  # How many daily backups to keep (keep_num for backwards compatbility)

# How many weekly backups to keep
bgbackup_keep_weekly: "3"

# How many monthly backups to keep
bgbackup_keep_monthly: "3"

# How many yearly backups to keep
bgbackup_keep_yearly: "2"

# How many log files to keep
bgbackup_keep_log_num: "1000"

# How many days to keep failed backups
bgbackup_keep_failed_days: 365

mariadb_version: "10.6"
bgbackup_tool_deb_package_name: "mariadb-backup-{{ mariadb_version }}"
bgbackup_tool_rpm_package_name: "MariaDB-backup"

# Michael 2022-09-08: It is recommended to keep your backup tool up to date.
# However, to work around bugs it may be necesarry to keep an older version of
# mariabackup. We therefore recommend to maintain the mariabackup package using
# the mariadb installation automation.
bgbackup_tool_package_state: "present"
# Other options: i
#  - 'latest', to install the latest version (through the package module)
#  - 'absent' to uninstall the package (through the package module)
#  - 'manually', to not touch the package at all (this is custom implementation in the role)

# WARNING: A backup is only a backup if you are sure you can restore it.
# We recommend automated restores, to restore every successful full backup.
#
# These are the settings for transferring the backup, although you may decide
# to transfer the backup differently. Please create a merge request if you
# implemented any improvements.
#
# Next to that, we recommend to verify the restored backup's consistency using
# a monitoring system. The monitoring system should check an imprtant table
# that is changed frequently and verify that recent (less then 48 hours old)
# data is present in the restored bakcup.
bgbackup_automated_restores: False

# This script is called after the backup is successfully created. This feature
# can be used without automated restores being enabled, but the default script
# is only deployed on the target machine when it is. Instead of using a script
# you could store the backup on a SAN and share the filesystem between the
# backup host and the restore host. NFS is not recommended for this purpose as
# it slows down the backup process significantly.
bgbackup_run_after_success_script: "{{ 'copy-last-backup.sh' if bgbackup_automated_restores | default(False) else None }}"
# This is the host name or IP address of the machine where the backup is restored.
# This host should be reachable through ansible, or the more specific configuration parameters can be used.
# bgbackup_restore_machine: "host.of.restore.machine"  # MANDATORY for automated restores to work.
# This is the host name or IP address of the restore machine that ansible uses to connect to.
bgbackup_restore_machine_ansible: "{{ bgbackup_restore_machine | default(None) }}"
# This is the host name or IP address of the restore machine the backup machine
# uses to connect to. The `copy-last-backup.sh` script uses the SSH protocol to
# copy the last backup to the restore machine.
bgbackup_restore_machine_ssh: "{{ bgbackup_restore_machine | default(None) }}"
# SSH user for the automated restore system. Mandatory when automted restores
# are enabled using `copy-last-backup.sh`.
bgbackup_restore_ssh_user: "bgrestore"
# Target directory for automated restore system. The backup process stores the
# last backup here (by default using `copy-last-backup.sh`), then the automated
# restore system takes over.
bgbackup_restore_target_directory: "/backups/tmp"  # WARNING: DO NOT ADD TRAILING SLASH

# Which path to store or generate the private key in
bgbackup_transfer_private_key_path: "{{ bgbackup_backup_path | default('/var/backups/bgbackup') }}/transfer_private_key{{ '-' + bgbackup_instance_name if bgbackup_instance_name else '' }}"

# By default, we generate an SSH key for backup transfers each time the system
# runs. This adds some extra security; if the key gets stolen someone would be
# able to steal the backup, but only until the script runs again. On first run,
# when no custom SSH keys are specified, SSH keys will be created anyways.
bgbackup_regenerate_transfer_ssh_keys: "{{ not bgbackup_transfer_ssh_private_key | default(None) }}"

# By default we empty the receiving account's authorized_keys file. When using
# a custom account you may want to decide to keep the original authorized_keys
# file.
bgrestore_empty_authorized_keys_file: True

# Custom private key file for file transfer.
# bgbackup_transfer_ssh_private_key: |
#   -----BEGIN OPENSSH PRIVATE KEY-----
#   .... content
#   .... of
#   .... private
#   .... key
#
# When specifying a private key you must specify the corresponding public key as well. All other authorized keys are 
# bgrestore_scp_public_key: "ssh-id-rsa ..... comment"

# Michael 2024-07-18: By default we email all backup logs. Options: all, none,
# failure. Invalid options will be 'none'.
bgbackup_mailon: "none"

# Which username is to be created on the restore host to receive the backups
bgrestore_scp_receive_user_username: "bgrestore"
# Which group is the primary group of that user
bgrestore_scp_receive_user_group: "backup"


# Settings for bgrestore system
bgrestore_restore_host: "{{ bgbackup_backup_host | default(False) }}"
bgrestore_restore_port: "{{ bgbackup_backup_port | default(False) }}"
bgrestore_restore_user: "{{ bgbackup_backup_user | default('backupuser') }}"
bgrestore_restore_password: "{{ bgbackup_backup_password | default('backuppassword') }}"

# Which directory to prepare the backup in
bgrestore_prepare_directory: "{{ bgbackup_restore_target_directory | default('/backups/tmp') }}"

# Where to find bgbackup history information
bgrestore_history_host: "{{ ansible_fqdn }}"
bgrestore_history_port: 3306
bgrestore_history_user: "{{ bgbackup_history_user | default(bgbackup_backup_user) }}"
bgrestore_history_password: "{{ bgbackup_history_password | default(bgbackup_backup_password) }}"
bgrestore_history_schema: "{{ bgbackup_history_schema | default('dba') }}"

# The host name of the machine being backed up for this automated restore
bgrestore_backup_source_host: "{{ ansible_fqdn }}"

bgrestore_target_directory: "{{ mariadb_datadir | default('/var/lib/mysql') }}"
bgrestore_datadir_owner: "mysql"
bgrestore_datadir_group: "mysql"
bgrestore_log_path: "/var/log/bgrestore"
bgrestore_notification_emails: "{{ bgbackup_notification_emails | default('michael@chipta.com') }}"

# Enable parallel backup/compression
bgbackup_backup_parallel_enabled: "yes"

# How many threads to create for backup/compression
bgbackup_backup_parallel_threads: 4

# At 7 minutes past 2 we create and transfer the backup, at 5 minutes past 7 we
# restore the backup. Please change this timing if your backup takes longer.
bgrestore_minute: "5"
bgrestore_hour: "7"  

# Before starting to decrypt and decompress, delete the old restored backup first (this saves the compression ratio of 1 data dir)
bgrestore_delete_previous_backup_first: False

# The backup process can call a script after a successful backup. This script may need a PATH set up in the cron job.
bgbackup_cron_path: "/usr/sbin:/usr/sbin:/usr/bin:/sbin:/bin"
bgrestore_cron_path: "{{ bgbackup_cron_path }}"

# Michael 2024-07-18: By default we email all bgrestore logs the same as
# bgbackup. Options: all, none, failure. Invalid options will be 'none'.
bgrestore_mailon: "{{ bgbackup_mailon | default('all') }}"

# How many log files to keep
bgrestore_keep_log_num: "{{ bgbackup_keep_log_num }}"

# Delete cron entries, credential files, configuration file and script
bgbackup_deconstruct: False
